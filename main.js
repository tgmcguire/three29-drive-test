//////////////////////////////
/// Tommy McGuire
/// tommy@modernmcguire.com
/// 330 518 4105
//////////////////////////////

// include packages
var fs 			= require('fs');
var express 	= require('express');
var async 		= require('async');
var google 		= require('googleapis');
var request 	= require('request');
var jade 		= require('jade');

var main = {};

// check for credentials
fs.readFile('./credentials.json', 'utf8', function(err, credentials) {
	if (err) {
		console.error("[ERROR] Google credentials 'credentials.json' is missing.");
		process.exit(1);
		return;
	};

	main.credentials = JSON.parse(credentials);

	if (typeof(main.credentials.web) === 'undefined' || 
		typeof(main.credentials.web.client_id) === 'undefined' || 
		typeof(main.credentials.web.auth_uri) === 'undefined' || 
		typeof(main.credentials.web.token_uri) === 'undefined' || 
		typeof(main.credentials.web.client_secret) === 'undefined' || 
		typeof(main.credentials.web.redirect_uris) === 'undefined') {
		console.error("[ERROR] Google credentials 'credentials.json' is malformed or missing data.");
		process.exit(1);
		return;
	};

	launch();
});

function launch() {
	// set up express and oauth client
	var app = express();
	var router = express.Router(); 
	app.use('/', router);
	app.set('view engine', 'jade');

	var oauth2 = google.auth.OAuth2;
	var oauth2_client = new oauth2(
		main.credentials.web.client_id,
		main.credentials.web.client_secret,
		main.credentials.web.redirect_uris[0]
	);

	google.options({auth: oauth2_client});

	var drive = google.drive('v3');

	// routes
	router.route('/').get(function(req, res) {
		res.render('_main.jade', {templateRender: jade.renderFile, jade_file: 'home', url: oauth2_client.generateAuthUrl({scope: 'https://www.googleapis.com/auth/drive'})});
	});

	router.route('/authorized').get(function(req, res) {
		if (typeof(req.query.code) === 'undefined') {
			res.redirect("/");
			return;
		}

		async.series([
			// get token
			function(callback) {
				oauth2_client.getToken(req.query.code, function(err, tokens) {
					if (err) {
						console.error(err);
						return;
					};

					main.token = tokens.access_token;
					callback();
				})
			},
			function(callback) {
				res.render('_main.jade', {templateRender: jade.renderFile, jade_file: 'update'});
			}
		])
	});

	router.route('/update').get(function(req, res) {
		async.series([
			// set credentials
			function(callback) {
				if (typeof(main.token) === 'undefined') {
					res.redirect("/");
					return;
				};

				oauth2_client.setCredentials({
					access_token: main.token
				});

				callback();
			},
			// create Folder 1
			function(callback) {
				drive.files.create({
					resource: {
						name: 'Folder 1',
						mimeType: 'application/vnd.google-apps.folder'
					},
					fields: 'id'
				}, function(err, file) {
					if (err) {
						console.error(err);
						return;
					};

					main.folder1_id = file.id;
					callback();
				})
			},
			// create Folder 2
			function(callback) {
				drive.files.create({
					resource: {
						name: 'Folder 2',
						mimeType: 'application/vnd.google-apps.folder',
						parents: [main.folder1_id]
					},
					fields: 'id'
				}, function(err, file) {
					if (err) {
						console.error(err);
						return;
					};

					main.folder2_id = file.id;
					callback();
				})
			},

			// get user info
			function(callback) {
				drive.about.get({fields: 'user'}, function(err, body) {
					main.user = body.user;
					callback();
				})
			},
			// save picture temporarily memory
			function(callback) {
				request(main.user.photoLink).pipe(fs.createWriteStream('_tmp.jpg').on('close', function() {
					callback();
				}));
			},
			// upload profile picture
			function(callback) {
				drive.files.create({
					resource: {
						name: main.user.displayName+".jpg",
						mimeType: "image/jpeg",
						parents: [main.folder2_id]
					},
					media: {
						mimeType: "image/jpeg",
						body: fs.createReadStream("_tmp.jpg")
					}
				}, function(err, file) {
					if (err) {
						console.error(err);
						return;
					};

					// remove picture
					fs.unlink('_tmp.jpg');

					console.log("Created structure for "+main.user.displayName);

					callback();
				})
			},
			function(callback) {
				res.render('_main.jade', {templateRender: jade.renderFile, jade_file: 'success'});
			}
		])
	})

	// routes are set up -- start the server
	var port = process.env.PORT || 3293;
	app.listen(port);
	console.log('✓ Three29 Developer Test is running on port ' + port);
} 