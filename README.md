# Three29 – Developer Test for Tommy McGuire
## Setup
- Create a project within Google Developer Console with Google Drive access and OAuth 2.0 credentials. The server is preconfigured to run on `http://127.0.0.1:3293`, with `http://127.0.0.1:3293/authorized` being the callback/referrer URL.
- Within the Credentials panel, click the "Download JSON" button under "OAuth 2.0 client IDs", and save this as `credentials.json` in the same folder as the project. *(Note: the script will throw an error if it cannot locate this file.)*
## Runtime
1. Run `npm install` in your project folder
2. Run `node main.js`
4. Once startup is complete, launch `http://127.0.0.1:3293` in your browser.
5. Follow the prompts to update your Drive.
## Contact Information
tommy@modernmcguire.com
330 518 4105